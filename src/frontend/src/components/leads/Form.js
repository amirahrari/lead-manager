import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { createLead } from "../../actions/leadsActions";

export class Form extends Component {
  static propTypes = {
    createLead: PropTypes.func.isRequired
  };

  renderInput({ input, type }) {
    return <input className="form-control" type={type} {...input} />;
  }

  renderTextArea({ input, type }) {
    return <textarea className="form-control" type={type} {...input} />;
  }

  onSubmit = formValues => {
    this.props.createLead(formValues);
    this.props.reset("leadForm");
  };

  render() {
    return (
      <div className="card card-body mt-4 mb-4">
        <h2>Add Lead</h2>
        <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
          <div className="form-group">
            <label>Name</label>
            <Field name="name" component={this.renderInput} type="text" />
          </div>
          <div className="form-group">
            <label>Email</label>
            <Field name="email" component={this.renderInput} type="email" />
          </div>
          <div className="form-group">
            <label>Message</label>
            <Field name="message" component={this.renderTextArea} type="text" />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}

const wrapped = reduxForm({
  form: "leadForm"
})(Form);

export default connect(
  null,
  { createLead }
)(wrapped);
