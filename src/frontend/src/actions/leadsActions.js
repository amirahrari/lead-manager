import axios from "axios";

import { createMessage } from "./messagesActions";
import { GET_LEADS, DELETE_LEAD, CREATE_LEAD, GET_ERRORS } from "./types";

export const getLeads = () => dispatch => {
  axios
    .get("/api/leads/")
    .then(response => {
      dispatch({ type: GET_LEADS, payload: response.data });
    })
    .catch(err => console.log(err));
};

export const deleteLead = id => dispatch => {
  axios
    .delete(`/api/leads/${id}/`)
    .then(() => {
      dispatch(createMessage({ deleteLead: "Lead Deleted." }));
      dispatch({
        type: DELETE_LEAD,
        payload: id
      });
    })
    .catch(err => console.log(err));
};

export const createLead = lead => dispatch => {
  axios
    .post("/api/leads/", lead)
    .then(response => {
      dispatch(createMessage({ createLead: "Lead Created." }));
      dispatch({ type: CREATE_LEAD, payload: response.data });
    })
    .catch(err => {
      const errors = {
        msg: err.response.data,
        status: err.response.status
      };
      dispatch({ type: GET_ERRORS, payload: errors });
    });
};
