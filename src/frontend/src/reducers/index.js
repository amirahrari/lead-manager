import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import leadsReducer from "./leadsReducer";
import errorsReducer from "./errorsReducer";
import messagesReducer from "./messagesReducer";

export default combineReducers({
  leads: leadsReducer,
  form: formReducer,
  errors: errorsReducer,
  messages: messagesReducer
});
