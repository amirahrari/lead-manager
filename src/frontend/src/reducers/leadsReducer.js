import { GET_LEADS, DELETE_LEAD, CREATE_LEAD } from "../actions/types";

const initialState = {
  leads: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LEADS:
      return { ...state, leads: action.payload };

    case DELETE_LEAD:
      return {
        ...state,
        leads: state.leads.filter(lead => lead.id != action.payload)
      };

    case CREATE_LEAD:
      return { ...state, leads: [action.payload, ...state.leads] };

    default:
      return state;
  }
};
