from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('api/leads', views.LeadViewSet, 'leads')

urlpatterns = router.urls
